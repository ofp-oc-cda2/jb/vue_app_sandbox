import { createRouter, createWebHistory } from 'vue-router';
import Home from '@/views/Home.vue';
import groupe from '@/views/groupe.vue';
import rdvs from '@/views/rdvs.vue';




const routes = [
    {
        name: 'Sessions client',
        path: '/groupe',
        component: groupe,
    }, {
        name: 'Accueil',
        path: '/Home',
        component: Home,
    }, {
        name: 'RDVs',
        path: '/rdvs',
        component: rdvs
    }
    
];




const router = createRouter ({
    history: createWebHistory(),
    routes,
})


export default router;
